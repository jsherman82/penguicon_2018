import pygame
import random

# Define colors
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)

# Intialize Pygame
pygame.init()
 
# Set the screen resolution
resolution = [800, 600]
screen = pygame.display.set_mode(resolution)

# Set the Window title
pygame.display.set_caption("Penguicon!!!")
 
# Loop until the user clicks the close button.
finished = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# Set starting position of square to a random x/y location
rect_y = random.randrange(50, resolution[1])
rect_x = random.randrange(50, resolution[0])

# Speed and direction of rectangle
rect_change_x = 8
rect_change_y = 8
 
# Main game loop
while not finished:
    # At the start of each loop, we check to see if we should exit
    for event in pygame.event.get():
        # Did we click the close button? If we did, set finished to True
        if event.type == pygame.QUIT:
            finished = True
 
    # Every "tick" of the clock we move the square
    rect_x += rect_change_x
    rect_y += rect_change_y
 
    # Change the direction if we're beyond the screen
    if rect_y > resolution[1] - 100  or rect_y < 0:
        rect_change_y = rect_change_y * -1
    if rect_x > resolution[0] - 100 or rect_x < 0:
        rect_change_x = rect_change_x * -1
 
    # Fill the screen with the desired color
    screen.fill(BLACK)
 
    # Draw the rectangle
    pygame.draw.rect(screen, GREEN, [rect_x, rect_y, 100, 100])
 
    # Set FPS
    clock.tick(60)
 
    # Update the screen with everything we've drawn
    pygame.display.flip()
 
# If we leave the main game loop, we end up here, where we just quit.
pygame.quit()
