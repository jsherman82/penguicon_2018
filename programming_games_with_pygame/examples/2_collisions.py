# Simple bullet example

import pygame
import random
 
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)

# Initialize Pygame
pygame.init()
 
# Set the resolution of the screen
resolution = [800, 600]
screen = pygame.display.set_mode(resolution)

# Set the window title
pygame.display.set_caption("Second Penguicon example {}".format(score))

# The following class is used to create blocks
class Block(pygame.sprite.Sprite):
    def __init__(self, color):
        # Call the parent class (Sprite) constructor
        super().__init__()
 
        self.image = pygame.Surface([20, 15])
        self.image.fill(color)
 
        self.rect = self.image.get_rect()

# The following class creates the player object
class Player(pygame.sprite.Sprite):
    def __init__(self):
        """ Set up the player on creation. """
        # Call the constructor of the parent class
        super().__init__()
 
        self.image = pygame.Surface([20, 20])
        self.image.fill(GREEN)
 
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Update the player's position. """
        # Get the current position of the mouse
        pos = pygame.mouse.get_pos()
 
        # Set the player location to match where the mouse is
        self.rect.x = pos[0]

# The following class is used to create bullets
class Bullet(pygame.sprite.Sprite):
    def __init__(self):
        # Call the constructor of the parent class
        super().__init__()
 
        self.image = pygame.Surface([4, 10])
        self.image.fill(WHITE)
 
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Move the bullet. """
        self.rect.y -= 8

# This is a list of every sprite. All blocks and the player block as well.
all_sprites_list = pygame.sprite.Group()

# A pygame "sprite group" is a container class, that acts kind of like an array
# List of each block in the game
block_list = pygame.sprite.Group()
 
# List of each bullet
bullet_list = pygame.sprite.Group()
 
for i in range(50):
    # Create an instance of the block class, set the color to blue
    block = Block(BLUE)
 
    # Set a random location for the block
    block.rect.x = random.randrange(resolution[0])
    block.rect.y = random.randrange(resolution[1] - 100) # Subtracting keeps the enemies from going off screen
 
    # Add the block to the list of objects
    block_list.add(block)
    all_sprites_list.add(block)
 
# Create a player block
player = Player()
all_sprites_list.add(player)
 
# Loop until the user clicks the close button.
finished = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Declare the score variable
score = 0

# Set y coordinate of the player (x will follow mouse)
player.rect.y = resolution[1] - 50
 
# Main loop
while not finished:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            # If the player has clicked the close button, we set this to true
            finished = True
 
        elif event.type == pygame.MOUSEBUTTONDOWN:
            # Create an instance of a bullet from the Bullet() class
            bullet = Bullet()
            # Set the bullet to start at the player location
            bullet.rect.x = player.rect.x + 10
            bullet.rect.y = player.rect.y - 2
            # Add the bullet to the lists of sprites
            all_sprites_list.add(bullet)
            bullet_list.add(bullet)
 
    # Call the update() method on all the sprites
    all_sprites_list.update()
 
    # Calculate mechanics for each bullet
    for bullet in bullet_list:
 
        # See if it hit a block
        block_hit_list = pygame.sprite.spritecollide(bullet, block_list, True)
 
        # For each block hit, remove the bullet and add to the score
        for block in block_hit_list:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
            score += 1
            print(score)
 
        # If the bullet leaves the screen, remove it
        if bullet.rect.y < - 10:
            bullet_list.remove(bullet)
            all_sprites_list.remove(bullet)
 
    # Fill the screen with a color
    screen.fill(BLACK)
 
    # Draw all the spites
    all_sprites_list.draw(screen)
 
    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # Set frames per second
    clock.tick(60)

# Exit
pygame.quit()
