#!/usr/bin/env python3

"""
 Space shooter game

 Inspired by code from http://programarcadegames.com

 Author: Jay LaCroix
 Title screen music: downloaded from opengameart.org, joth
 Stage background music: downloaded from opengameart.org, author celestialghost

"""

import pygame
import random
import sys

# Initialize the game engine
pygame.init()

# Custom modules
from modules import artillery
from modules import audio
from modules import config as c
from modules import player
from modules import power_up

# Loop 50 times and add a star in a random x,y position
for i in range(100):
    x = random.randrange(0, c.RESOLUTION[0])
    y = random.randrange(0, c.RESOLUTION[0])
    c.star_list.append([x, y])

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Player ship object
player_ship = player.Ship()  # spawn ship
player_ship.rect.x = c.RESOLUTION[1]/2  # starting position of character on screen
player_ship.rect.y = 515  # starting position of character on screen
player_ship_list = pygame.sprite.Group()
player_ship_list.add(player_ship)

# Play background music
audio.bgm()

# Loop until the user clicks the close button.
done = False

while not done:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT or event.key == ord('a'):
                player_ship.control(-c.steps,0)
            if event.key == pygame.K_RIGHT or event.key == ord('d'):
                player_ship.control(c.steps,0)
            if event.key == pygame.K_UP or event.key == ord('w'):
                audio.laser_sound.play()
                player_bullet_left = artillery.Player_Bullet()
                player_bullet_right = artillery.Player_Bullet()

                # Center the bullets on the sprite:
                player_bullet_left.rect.x = player_ship.rect.x  + 16
                player_bullet_right.rect.x = player_ship.rect.x + 22

                # Set how far above the ship sprite the bullets are drawn:
                player_bullet_left.rect.y = player_ship.rect.y  + -4
                player_bullet_right.rect.y = player_ship.rect.y + -4

                # Add the bullets to the lists
                c.all_sprites_list.add(player_bullet_left)
                c.all_sprites_list.add(player_bullet_right)
                c.bullet_list.add(player_bullet_left)
                c.bullet_list.add(player_bullet_right)

                if c.ship_power_up_1 == True:
                    player_bullet_extra_left = artillery.Player_Bullet()
                    player_bullet_extra_right = artillery.Player_Bullet()

                    # Make the bullets draw near the sprite's cannons
                    player_bullet_extra_left.rect.x = player_ship.rect.x + 2
                    player_bullet_extra_right.rect.x = player_ship.rect.x + 35

                    # Make the bullets draw just above the cannons
                    player_bullet_extra_left.rect.y = player_ship.rect.y + 30
                    player_bullet_extra_right.rect.y = player_ship.rect.y + 30

                    c.all_sprites_list.add(player_bullet_extra_left)
                    c.all_sprites_list.add(player_bullet_extra_right)
                    c.bullet_list.add(player_bullet_extra_left)
                    c.bullet_list.add(player_bullet_extra_right)

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == ord('a'):
                player_ship.control(c.steps,0)
            if event.key == pygame.K_RIGHT or event.key == ord('d'):
                player_ship.control(-c.steps,0)
            if event.key == ord('q'):
                pygame.quit()
                sys.exit()
                main = False

    c.screen.fill(c.BLACK)

    # Will potentially draw powerups
    power_up.draw_power_ups()

    # Call the update() method on all the sprites
    c.all_sprites_list.update()

    for i in range(len(c.star_list)):
        # Draw the star
        twinkle = random.randrange(3)
        pygame.draw.circle(c.screen, c.WHITE, c.star_list[i], twinkle)

        # Move the star down one pixel
        c.star_list[i][1] += 1

        # If the star has moved off the bottom of the screen
        if c.star_list[i][1] > 600:
            # Reset it just above the top
            y = random.randrange(-50, -10)
            c.star_list[i][1] = y
            # Give it a new x position
            x = random.randrange(0, c.RESOLUTION[0])
            c.star_list[i][0] = x

    for bullet in c.bullet_list:
        # Remove the bullet if it goes beyond the screen
        if bullet.rect.y < -10:
            c.bullet_list.remove(bullet)
            c.all_sprites_list.remove(bullet)

    # See if the ship has collided with a powerup
    powerup_hit_list = pygame.sprite.spritecollide(player_ship, c.powerup_list, True)
    for powerup in powerup_hit_list:
        c.score += 100
        ship_power_up_1 = True
        print("Score: {}".format(c.score))
    

    # Go ahead and update the screen with what we've drawn.
    player_ship.update()
    player_ship_list.draw(c.screen)
    c.all_sprites_list.draw(c.screen)
    pygame.display.flip()
    clock.tick(c.FPS)
 
# Close the window and quit.
pygame.quit()
