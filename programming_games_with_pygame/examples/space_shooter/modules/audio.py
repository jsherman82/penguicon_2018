#!/usr/bin/python3

"""
Space shooter game
Audio module
"""

import os
import pygame

# Sound effects
laser_sound = pygame.mixer.Sound(os.path.join('resources','sounds','laser.ogg'))

# background music
def bgm():
    pygame.mixer.music.load(os.path.join('resources','sounds','stage_bg.wav'))
    pygame.mixer.music.set_endevent(pygame.constants.USEREVENT)

    pygame.mixer.music.play(-1)
