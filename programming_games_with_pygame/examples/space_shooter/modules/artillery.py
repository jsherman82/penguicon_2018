#!/usr/bin/python3

"""
 Space shooter game

 artillery module
"""

import pygame

# Custom modules
from . import config as c

class Player_Bullet(pygame.sprite.Sprite):
    """ This class represents the bullet . """
    def __init__(self):
        # Call the parent class (Sprite) constructor
        super().__init__()
 
        self.image = pygame.Surface([2, 6])
        self.image.fill(c.WHITE)
 
        self.rect = self.image.get_rect()
 
    def update(self):
        """ Move the bullet. """
        self.rect.y -= 15
